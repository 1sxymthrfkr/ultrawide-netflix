var enabled = false;
var modifiedTabId;
var currentMode;

browser.browserAction.setBadgeBackgroundColor({
    color: "green"
});

function onError(error) {
    console.log(error);
}

function sendMessageToTabs(tabs) {
    for (let tab of tabs) {
        if (tab.url.includes("netflix.com/watch/")) {
            modifiedTabId = tab.id;
            browser.tabs
                .sendMessage(tab.id, {
                    mode: currentMode
                })
                .then(response => {
                    enabled = !enabled;
                    if (enabled) {
                        browser.browserAction.setBadgeText({
                            text: "✔"
                        });
                    } else {
                        browser.browserAction.setBadgeText({
                            text: ""
                        });
                    }
                })
                .catch(onError);
        }
    }
}

browser.browserAction.onClicked.addListener(() => {
    currentMode = "toggle-zoom";
    browser.tabs
        .query({
            currentWindow: true,
            active: true
        })
        .then(sendMessageToTabs)
        .catch(onError);
});

browser.commands.onCommand.addListener(command => {
    currentMode = command;
    browser.tabs
        .query({
            currentWindow: true,
            active: true
        })
        .then(sendMessageToTabs)
        .catch(onError);
});

browser.tabs.onRemoved.addListener((tabId, removeInfo) => {
    enabled = false;
    if (tabId === modifiedTabId) {
        browser.browserAction.setBadgeText({
            text: ""
        });
    }
});

browser.tabs.onUpdated.addListener(() => {
    browser.tabs
        .query({
            currentWindow: true,
            active: true
        })
        .then(tabs => {
            for (let tab of tabs) {
                if (!tab.url.includes("netflix.com/watch/")) {
                    enabled = false;
                    browser.browserAction.setBadgeText({
                        text: ""
                    });
                }
            }
        })
        .catch(onError);
});
